require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get academic" do
    get :academic
    assert_response :success
  end

  test "should get admision" do
    get :admision
    assert_response :success
  end

  test "should get contacts" do
    get :contacts
    assert_response :success
  end

  test "should get news" do
    get :news
    assert_response :success
  end

  test "should get structure" do
    get :structure
    assert_response :success
  end

end
