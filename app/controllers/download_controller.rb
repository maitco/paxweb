class DownloadController < ApplicationController
 
 def pdf
    send_file Rails.root.join('data', 'results.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def pdf2
    send_file Rails.root.join('data', 'fee-structure.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def doc
    send_file Rails.root.join('data', 'application-form.docx'), :type=>"application/doc", :x_sendfile=>true
  end
end
